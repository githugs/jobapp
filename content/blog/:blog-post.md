---
title: "Blog"
date: 2021-03-18T02:22:23Z
slug: "Blog"
description: "I currently blog @ the art of network eng"
keywords: ["blog", "words"]
draft: false
tags: [blog]
math: false
toc: false
---

# Blogging

It seems like we all do it, or don't we?  

Blogging for me is a way I learn.  Trying to explain something I just did or experienced with words.  

Currently I do all my blogging @ [The Art of Network Engineering](https://artofnetworkengineering.com/author/roberge2/)

I'm open to doing more blogging, technical writing or social media management for $$$ at some point.  
But for now I'm just into learning and sharing :)
